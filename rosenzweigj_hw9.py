###########################################################################################################
## PURPOSE: Determine the mass of a far away black hole (BH), based on the hydrogen alpha emission spectrum
##          given off by the gas accreted by the BH. Use a double Gaussian method to fit the emission line.
## SYNTAX:  python <script.py>
## NOTES:   
## AUTHOR:  Jake Rosenzweig
## DATE:    2018-12-08
###########################################################################################################
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#####################
##### Constants #####
#####################
c = 299792458 # m/s
Msun = 1.9884754E30 # Kg
Koss_massBH = 5.5E+06*Msun

######################
##### Parameters #####
######################
d = 155 # Mpc, distance from BH to Earth
Halpha_flux = 0.242E-15 # W/(m^2)

with open('halpha.dat') as f:
    ## flux [W/(m^2)]
    wavelen_arr, flux_arr = np.loadtxt(f, unpack=True)

## Renormalize the data so that min flux = 0 
## also use values of flux on the order(10^1)
normflux_arr = ( flux_arr - flux_arr.min() )*1E15


#####################
##### Functions #####
#####################
def gauss(x,A,mean,sigma):
    return A/(np.sqrt(2*np.pi)*sigma) * np.exp( -1/2*(x-mean)*(x-mean)/sigma/sigma )

def doubleGauss(x,A1,mean1,sigma1,A2,mean2,sigma2):
    gauss1 = A1/(np.sqrt(2*np.pi)*sigma1) * np.exp( -1/2*(x-mean1)*(x-mean1)/sigma1/sigma1 )
    gauss2 = A2/(np.sqrt(2*np.pi)*sigma2) * np.exp( -1/2*(x-mean2)*(x-mean2)/sigma2/sigma2 )
    return gauss1 + gauss2

def massBH(lumi,fwhm):
    '''
    Using the luminosity (lumi) of the broad Hydrogen alpha line in Watts and the full-width 
    half-max (fwhm) of the emission line, calculate the mass of the black hole which is giving 
    off this radiation by the accretion of nearby gas.
    
    Strangely enough, we will convert the fwhm into velocity units...
    
    Arguments:
        lumi [W]
        fwhm [Km/s] (use fwhm/wavelength = v/c)
    '''
    return 1.3E6 * (lumi/1E35)**0.57 * (fwhm/1E3)**2.06 * Msun

def Plotter(newpopt):
    gauscurve2 = doubleGauss(x,newpopt[0],newpopt[1],newpopt[2],newpopt[3],newpopt[4],newpopt[5])
    
    plt.scatter(wavelen_arr,normflux_arr)
    plt.xlim(wavelen_arr.min()*0.999, wavelen_arr.max()*1.001)
    plt.ylim(normflux_arr.min()*0.999, normflux_arr.max()*1.001)
    plt.xlabel('$wavelength\ [Å]$')
    plt.ylabel("$flux of H\Alpha line \n [W/(m^2)]$")
    plt.plot(x,gauscurve,color='r')
    plt.plot(x,gauscurve2,color='k')
    plt.show()


## Fit a Gaussian curve to the data
x = np.linspace(wavelen_arr.min(),wavelen_arr.max(),5000)

## Get optimized parameters for Gaussian
popt,pcov = curve_fit(gauss,wavelen_arr,normflux_arr,p0=(2500, 6560, 50))
gauscurve = gauss(x,popt[0],popt[1],popt[2])

print("The optimized parameters are:")
print("A:\t%.6E"%popt[0],"[W/(m^2)]")
print("mean:\t%.6E"%popt[1],"[m]")
print("sigma:\t%.6E"%popt[2],"[m]")

## Plot it to make sure the fit is OK
#plt.scatter(wavelen_arr,normflux_arr)
#plt.xlim(wavelen_arr.min()*0.999, wavelen_arr.max()*1.001)
#plt.ylim(normflux_arr.min()*0.999, normflux_arr.max()*1.001)
#plt.xlabel('$wavelength\ [m]$')
#plt.ylabel("flux of H_Alpha line \n [W/(m^2)]")
#plt.plot(x,gauscurve,color='r')
#plt.show()


## Fit data with a DOUBLE Gaussian
popt2,pcov2 = curve_fit(doubleGauss,wavelen_arr,normflux_arr,p0=(250, 6564.15, 19, 12, 6564.15, 2.7))
print("The optimized parameters of the broad curve are:")
print("\tsingle Gaussian\tdouble Gaussian")
print("A:\t%.6E"%popt[0],"\t%.6E"%popt2[0],"[W/(m^2)]")
print("mean:\t%.6E"%popt[1],"\t%.6E"%popt2[1],"[m]")
print("sigma:\t%.6E"%popt[2],"\t%.6E"%popt2[2],"[m]")

gauscurve2 = doubleGauss(x,popt2[0],popt2[1],popt2[2],popt2[3],popt2[4],popt2[5])

## Plot it to make sure the fit is OK
plt.scatter(wavelen_arr,normflux_arr,label='data')
plt.xlim(wavelen_arr.min()*0.999, wavelen_arr.max()*1.001)
plt.ylim(normflux_arr.min()*0.999, normflux_arr.max()*1.001)
plt.xlabel('$wavelength\ [Å]$')
plt.ylabel("flux of H_Alpha line \n [W/(m^2)]")
plt.plot(x,gauscurve,color='r',label='single Gaussian')
plt.plot(x,gauscurve2,color='k',label='double Gaussian')
plt.legend()
plt.show()


print("""
The fit with a double Gaussian is quite better, even judging by eye. 
The curve captures either the two "wings" out at the side or the narrow peak at the top.
We could try a triple Gaussian to get an even better fit.
This is encroaching in the area of "Gaussian Mixture Modeling" 
and it is most likely an entirely different project!
""")


## Sample parameter space of amplitude and sigma to search for the mysterious "second Gaussian"
## This Gaussian should fit the "wings" on the left and right, and also the narrow peak
# for amp in np.arange(0.5,100,0.5):
#     for sigma in np.arange(0.5,200,0.5):
#         try:
#             newpopt,pcov = curve_fit(doubleGauss,wavelen_arr,normflux_arr,p0=(amp, 6564.15, sigma, 1/amp, 6564.15, 1/sigma))
#             if (abs(1.781015E+01-newpopt[2])>1) and newpopt[2]>0 and newpopt[0]>0 and abs(newpopt[1]-6564.15)<1:
#                 print("Using amplitude={}, sigma={} new parameters: {}".format(amp,sigma,newpopt))
#                 Plotter(newpopt)
                
#         except (ValueError, RuntimeError):
#             continue
# print("Done!")


## Find the FWHM (full-width half max) in Km/s using the first, broad Gaussian
fwhm = 2*popt[2]*np.sqrt(2*np.log(2)) # popt=sigma, which is in Å!
#fwhm = temp_fwhm/1E13

## Also find the x val which gives the max y val
# max_yval = gauscurve.max()
# max_xval = x[gauscurve.argmax()]

## Convert the FWHM into the speed of the gas
v = c*( fwhm/popt[1] )/1E3 # convert to Km/s

## Convert the observed flux into lumi:
## lumi = 4pi*d^2 * flux
dist = d*(3.09E22) # convert Mpc to m
lumi = 4*np.pi*dist*dist * Halpha_flux


## Calculate mass of BH!
mass_BH = massBH(lumi,v)

percdiff = (mass_BH - Koss_massBH)/((mass_BH + Koss_massBH)/2) * 100

print("""
Using the Hydrogen alpha emission line data from a black hole (BH),
which is approximately {0} Mpc away, and based on its total observed flux of {1} W/(m^2)
as measured on Earth, we have determined that the gas nearest the BH is traveling near 
{2:.2f} Km/s and is giving off the Gaussian-like emission spectrum with a mean of {3:.2f} Å.
All this information allows us to calculate the mass of the BH to be around {4:.2E} Kg,
or equivalently, {5:.1E} solar masses.

This is quite close to the reported mass of the same BH from Koss et al. of {6:.2E} solar masses,
giving a percent difference of: {7:.2f}%. Not bad for acoupla Gaussian curves!
""".format(d, Halpha_flux, v, popt[1], mass_BH, mass_BH/Msun, Koss_massBH/Msun, percdiff))
